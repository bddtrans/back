# BddTrans Unofficial API
## why
while https://bddtrans.fr/accueil/ is an awesome tool for trans people to help them in they transition, it lack some functions
to get a better user experience.
- Filtering
- mobile-first interface
- open data

The goal of this project is to unofficially respond at this needs with a scrapper to periodically fetch data from the website,
an api to provide them to whoever need it, and a front providing a better user experience in mobile devices, implementing
search and filtering capabilities.

## Back
You're actually on the documentation for the backend of this project,  
This project actually consist in two main components:
- a Parser
    - to fetch data from the website and store it in database to make it accessible by the API
    - written in python
    - fetch each day at 00:00
    - use Beautifull Soup to parse the data from the HTML
- an Api
    - to provide the data for every service that need it
    - written in Rust for the fun
    - use Redis-rs to implement the search module of redis, fetch and parse the data
    - use Juniper and Rocket to handle graphql request and provide the fetched data

### Full Stack
#### Before you start

You will need:
- a running docker and docker compose

#### How to run the full stack using docker

1. create a docker-compose.override.yml file to map the port 8000 of the api on the port of your choice
    ```yaml
    version: "3"
    services:
      api:
        ports:
          - "8000:8000"
    ```
2. launch the docker compose stack

   ```bash
    docker compose up -d
   ```
3. run the parser to populate the db
    ```bash
        docker compose exec parser poetry run python parsing.py
    ```
4. go to http://localhost:8000

### Parser
#### what is it
The parser is a process that run every day at 00:00 to fetch the scrap each page of html coming from BddTrans,
extract the data from it and store it in the database using redis_om.
It can also be manually run using the parsing.py module.
#### Before you start
to run the parser you will need:
- python 3.11 at least
- a running redis instance

#### How to setup and run the parser

1. setup REDIS_OM_URL env var to match your redis instance uri or leave empty to use default

    ```bash
    $REDIS_OM_URL=redis://127.0.0.1:6379/db
    ```
2. setup your python env using poetry
   
    ```bash
   cd parser
    pip install poetry
    poetry install 
    ```
3. manually run the parser using the poetry managed env

    ```bash
    poetry run python parsing.py
    ```
4. run the periodic task using the poetry managed env

    ```bash
    poetry run python main.py 
   ```

### Api
#### what is it
the api will serve the data coming from the database over a graphql api than can be accessed on the port 8000
#### Before you start
to build and run the api you will need:
- the latest version of rustc nightly
- a running redis instance populated with data from the parser

1. setup REDIS_URL env var or leave empty to use default

    ```bash
     $REDIS_URL=redis://127.0.0.1:6379/db
   ```
2. use cargo to build and run the project

    ```bash
    cd api
   cargo run 
   ```
3. go to http://localhost:8000
4. run an exemple request
    ```graphql
    {
       kinds
   } 
   ```