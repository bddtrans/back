import requests as r
from bs4 import BeautifulSoup
from redis_om import Migrator, NotFoundError

from models import DoctorModel

BASE_URL = "https://bddtrans.fr/"
PAGES = [
    "generalistes-{}",
    "endocrinologues-{}",
    "psy-{}",
    "voix-{}",
    "chirugiens-{}",
    "gynecologues-{}",
    "dermato-{}",
    "avocats-{}",
    "autres-{}",
]


def parse_people(people):
    spans = people.find_all("span")
    strongs = people.find_all("strong")

    return DoctorModel(
        kind=spans[0].text,
        description=spans[1].text,
        first_name=strongs[1].text,
        last_name=strongs[0].text,
        address=strongs[2].text,
        postal_code=strongs[3].text,
        city=strongs[4].text,
        country=strongs[5].text,
        info_link=people.find(class_="view_prat_link").a["href"][2:],
    )


def parse_page(page):
    peoples = page.find_all(class_="summ_prat")
    for raw_people in peoples:
        new_people = parse_people(raw_people)
        try:
            expression = DoctorModel.kind == new_people.kind
            if new_people.last_name:
                expression = expression & (
                    DoctorModel.last_name == new_people.last_name
                )
            if new_people.first_name:
                expression = expression & (
                    DoctorModel.first_name == new_people.first_name
                )
            if new_people.city:
                expression = expression & (DoctorModel.city == new_people.city)

            old_people = DoctorModel.find(expression).first()
            new_people.pk = old_people.pk
        except NotFoundError:
            pass
        new_people.save()


def parse():
    Migrator().run()

    for page in PAGES:
        response = r.get(f"{BASE_URL}{page.format(1)}/")
        soup = BeautifulSoup(response.content, "html.parser")
        nb_pages = int([i for i in soup.find(class_="npages").children][-1].text)
        parse_page(soup)
        for i in range(2, nb_pages + 1):
            response = r.get(f"{BASE_URL}{page.format(i)}/")
            soup = BeautifulSoup(response.content, "html.parser")
            parse_page(soup)

    kind_list = ";".join({i.kind for i in DoctorModel.find().all()})
    DoctorModel.db().set(':TransDb.DoctorKindList', kind_list)


if __name__ == "__main__":
    parse()
