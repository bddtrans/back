import schedule
import time

from parsing import parse


def job():
    parse()


if __name__ == '__main__':
    schedule.every().day.at("00:00").do(job)

    while True:
        schedule.run_pending()
        time.sleep(1)
