from redis_om import HashModel, Field
from typing import Optional


class DoctorModel(HashModel):
    class Meta:
        model_key_prefix = "TransDb.Doctor"

    kind: str = Field(index=True)
    description: str
    first_name: str = Field(index=True, full_text_search=True)
    last_name: Optional[str] = Field(index=True)
    address: str
    postal_code: str = Field(index=True)
    city: str = Field(index=True)
    country: str
    info_link: str
