use juniper::{EmptyMutation, EmptySubscription, FieldResult, graphql_object, GraphQLInputObject, GraphQLObject};
use redis::{Client, Cmd, Commands, RedisResult, Value};
use crate::parsing::{Doctor, parse_response};
use std::env;

#[derive(GraphQLInputObject)]
struct DoctorFilters {
    last_name: Option<String>,
    first_name: Option<String>,
    kind: Option<String>,
    postal_code: Option<String>,
    city: Option<String>,
}

#[derive(GraphQLObject)]
pub struct PaginatedResponse {
    pub total: i32,
    pub data: Vec<Doctor>,
}

fn get_redis_client() -> RedisResult<Client> {
    let url = redis::parse_redis_url(match env::var("REDIS_URL"){
        Ok(v) => v,
        Err(_) => "redis://127.0.0.1/".to_owned()
    }.as_str());
    redis::Client::open(url.unwrap())
}

pub struct Query;

#[graphql_object]
impl Query {
    fn kinds() -> FieldResult<Vec<String>> {
        let client = get_redis_client()?;
        let mut con = client.get_connection()?;

        let result_string: String = con.get(":TransDb.DoctorKindList")?;
        let mut keys: Vec<String> = Vec::new();
        for key_str in result_string.split(";"){
            keys.push(key_str.to_owned());
        }
        Ok(keys)
    }
    fn doctors(offset: Option<i32>, nb: Option<i32>, filters: Option<DoctorFilters>) -> FieldResult<PaginatedResponse> {
        let client = get_redis_client()?;
        let mut con = client.get_connection()?;

        let mut cmd: Cmd = redis::cmd("FT.SEARCH");
        let mut query: &mut Cmd = cmd.arg(":TransDb.Doctor:index");

        query = match filters {
            Some(filters) => {
                let mut filters_string = String::new();
                filters_string.push_str(&match filters.postal_code {
                    Some(postal_code) => format!("@postal_code:{{{}}}", postal_code),
                    _ => String::new()
                });
                filters_string.push_str(&match filters.last_name {
                    Some(last_name) => format!("@last_name:{{{}}}", last_name),
                    _ => String::new()
                });
                filters_string.push_str(&match filters.first_name {
                    Some(first_name) => format!("@first_name:{{{}}}", first_name),
                    _ => String::new()
                });
                filters_string.push_str(&match filters.city {
                    Some(city) => format!("@city:{{{}}}", city),
                    _ => String::new()
                });
                filters_string.push_str(&match filters.kind {
                    Some(kind) => format!("@kind:{{{}}}", kind),
                    _ => String::new()
                });

                if filters_string.is_empty() {
                    query.arg("*")
                } else {
                    query.arg(filters_string)
                }
            }
            _ => query.arg("*")
        };

        let response: Value = query
            .arg("limit")
            .arg(match offset {
                None => { 0 }
                Some(offset) => { offset }
            })
            .arg(match nb {
                None => { 100 }
                Some(nb) => { nb }
            })
            .query(&mut con)?;

        Ok(match parse_response(response)? {
            Some(results) => results,
            None => PaginatedResponse {
                total: 0,
                data: Vec::new(),
            }
        })
    }
}

pub type Schema = juniper::RootNode<'static, Query, EmptyMutation, EmptySubscription>;
