use std::string::FromUtf8Error;
use redis_derive::{ToRedisArgs};

use juniper::{GraphQLObject};
use redis::{FromRedisValue, RedisResult, Value};
use crate::schema::PaginatedResponse;

#[derive(GraphQLObject)]
#[derive(ToRedisArgs, Debug)]
pub struct Doctor {
    // Graphql type to represent the DoctorModel items from the db
    pk: String,
    kind: String, // type
    description: String,
    first_name: String,
    last_name: String,
    address: String,
    postal_code: String,
    city: String,
    country: String,
    info_link: String
}

impl FromRedisValue for Doctor {
    fn from_redis_value(v: &Value) -> RedisResult<Self> {
        // custom implementation of from_redis_value to iterate over the value array coming from a bulk
        // data chunk from redis, each field is a string value structured like this:
        // [
        // field_name,
        // field_value,
        // ...
        // ]
        let values: Vec<String> = redis::from_redis_value(v)?;

        let mut pk = String::new();
        let mut kind = String::new();
        let mut description = String::new();
        let mut first_name = String::new();
        let mut last_name = String::new();
        let mut address = String::new();
        let mut postal_code = String::new();
        let mut city = String::new();
        let mut country = String::new();
        let mut info_link = String::new();

        for i in 0..values.len() {
            // iterate over each field
            match values[i].as_str() {
                // if a key string is matched,
                // set the value of the matching variable to the index n+1
                "pk" => pk = values[i+1].clone(),
                "kind" => kind = values[i + 1].clone(),
                "description" => description = values[i + 1].clone(),
                "first_name" => first_name = values[i + 1].clone(),
                "last_name" => last_name = values[i + 1].clone(),
                "address" => address = values[i + 1].clone(),
                "postal_code" => postal_code = values[i + 1].clone(),
                "city" => city = values[i + 1].clone(),
                "country" => country = values[i + 1].clone(),
                "info_link" => info_link = values[i + 1].clone(),
                _ => continue
            }
        }

        Ok(Doctor{
            pk,
            kind,
            description,
            first_name,
            last_name,
            address,
            postal_code,
            city,
            country,
            info_link
        })
    }
}

pub fn parse_response(response: Value) -> Result<Option<PaginatedResponse>, FromUtf8Error>{
    // parse the data coming from redis into a new PaginatedResponse struct
    // initially, i want to made this function generic, but too lazy.
    match response {
        Value::Bulk(array) => {
            // if response is a bulk Data, let's go
            let mut results = Vec::new();
            let mut data = array.iter(); // casting the bulk into an iterator over each value
            let n: &i64 = match data.next().unwrap() {
                Value::Int(n) => n,
                _ => panic!("wrong data format")
            }; // first value of the bulk data is the size of the set (all cursor merged), so we fetch it for later use
            /*
                bulk data is formated as follow:
                [
                    key_name: String
                    key_value: Value or Bulk
                ]
                so we need to iterate over each item, if it's a string, we store the key for alter use,
                and if it's a bulk, we parse it into a new struct
            */
            let mut key: Option<String> = None;
            for item in array.iter().skip(1) {
                match item {
                    Value::Bulk(ref bulk) => {
                        let mut bulk = bulk.clone(); // clone the bulk to avoid taking ownership ?
                        /*
                            if we found a bulk, we assume that we already have the matching key,
                            so we push it into the bulk to use it in the parsing function.

                            Eventually, it may be more reusable to have one bulk parsing function for each item type.
                         */
                        bulk.push(Value::Data(Vec::from("pk")));
                        bulk.push(Value::Data(Vec::from(key.clone().unwrap()))); // clone the key to avoid taking ownership ?
                        if let Ok(result) =
                            Doctor::from_redis_value(&Value::Bulk(bulk.clone())) // create a new Bulk enum and parse it into a new Doctor struct
                        {
                            // if ok, push it into the results vec
                            results.push(result);
                            key = None; // setting the key to None is not mandatory, but it help code flow understanding.
                        };
                    },
                    Value::Data(ref key_data) => {
                        key = Some(String::from_utf8(key_data.to_vec())?);
                    },
                    _ => (),
                };
            }
            Ok(Some(PaginatedResponse{
                total: i32::try_from(*n).ok().unwrap(),
                data: results
            }))
        },
        _ => Ok(None),
    }
}