use juniper::{EmptyMutation, EmptySubscription};
use juniper_rocket::GraphQLResponse;

use rocket::{Rocket, State};
use rocket::response::content::RawHtml;
use crate::schema::{Schema, Query};
use rocket::http::Header;
use rocket::{Request, Response};
use rocket::fairing::{Fairing, Info, Kind};

pub mod parsing;
pub mod schema;

#[rocket::get("/")]
fn graphiql() -> RawHtml<String> {
    juniper_rocket::graphiql_source("/graphql", None)
}

#[rocket::get("/graphql?<request>")]
fn get_graphql_handler(
    context: &State<()>,
    request: juniper_rocket::GraphQLRequest,
    schema: &State<Schema>,
) -> GraphQLResponse {
    request.execute_sync(&*schema, &*context)
}

#[rocket::post("/graphql", data = "<request>")]
fn post_graphql_handler(
    context: &State<()>,
    request: juniper_rocket::GraphQLRequest,
    schema: &State<Schema>,
) -> GraphQLResponse {
    request.execute_sync(&*schema, &*context)
}


#[rocket::options("/graphql")]
fn options_graphql_handler() {
    // handle pre flight requests from CORS check
}

pub struct CORS;

#[rocket::async_trait]
impl Fairing for CORS {
    fn info(&self) -> Info {
        Info {
            name: "Add CORS headers to responses",
            kind: Kind::Response
        }
    }

    async fn on_response<'r>(&self, _request: &'r Request<'_>, response: &mut Response<'r>) {
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new("Access-Control-Allow-Methods", "POST, GET, PATCH, OPTIONS"));
        response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));
    }
}

#[rocket::main]
async fn main() {

    let _ = Rocket::build()
        .manage(())
        .manage(Schema::new(
            Query,
            EmptyMutation::new(),
            EmptySubscription::new(),
        ))
        .mount(
            "/",
            rocket::routes![graphiql, get_graphql_handler, post_graphql_handler, options_graphql_handler],
        )
        .attach(CORS)
        .launch()
        .await
        .expect("server to launch");
}
